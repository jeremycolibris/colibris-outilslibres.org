import React from 'react'
import Link from 'gatsby-link'
import logo from './img/Logo_Colibris.svg'; // Tell Webpack this JS file uses this image
import './style.scss'; // Tell Webpack this JS file uses this image


class SiteNavi extends React.Component {
  render() {
    const { location, title } = this.props
    return (
      <nav className="navbar navbar-expand-lg navbar-default">
        <div className="container">
          <Link className="text-center" to="/">
            <h1 className="navbar-brand mb-0"><img src={logo} alt={title} /></h1>
          </Link>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation"
          onClick={() => document.getElementById('navbarToggler').classList.toggle('show')} >
            <i className="fa fa-bars" aria-hidden="true"></i> MENU
          </button>
          <div className="collapse navbar-collapse" id="navbarToggler">  
              <ul className="navbar-nav mx-auto">
                <li
                  className={
                    location.pathname === '/#top' || location.pathname === '/' ? 'nav-item active' : 'nav-item'
                  }
                >
                  <Link to="/#top" className="px-4 px-sm-3 nav-link">
                    Outils libres
                  </Link>
                </li>
                <li
                  className={
                    location.pathname === '/le-pourquoi-du-comment/'
                      ? 'nav-item active'
                      : 'nav-item'
                  }
                >
                  <Link to="/le-pourquoi-du-comment/" className="px-4 px-sm-3 nav-link">
                    Le pourquoi du comment
                  </Link>
                </li>
                <li
                  className={
                    location.pathname === '/actus/'
                      ? 'nav-item active'
                      : 'nav-item'
                  }
                >
                  <Link to="/actus/" className="px-4 px-sm-3 nav-link">
                    Actus
                  </Link>
                </li>
                <li
                  className={
                    location.pathname === '/des-questions/'
                      ? 'nav-item active'
                      : 'nav-item'
                  }
                >
                  <Link to="/des-questions/" className="px-4 px-sm-3 nav-link">
                    Des questions ?
                  </Link>
                </li>
              </ul>
            <form className="form-inline my-2">
              <a href="https://www.jedonneenligne.org/colibris/OUTILSLIBRES/" title="Merci de votre soutien!" className="btn btn-sm btn-danger my-2 my-sm-0 btn-donate"><i className="fa fa-heart"></i> Faire un don</a>
            </form>
          </div>
        </div>
      </nav>
    )
  }
}

export default SiteNavi
