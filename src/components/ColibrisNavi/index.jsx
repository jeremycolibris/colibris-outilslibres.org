import React from 'react'
import './style.scss'
// import don from './img/bandeau-AAD_SitesVF.jpg'

class ColibrisNavi extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      menu: {}
    }
  }
  componentDidUpdate() {
    document.getElementsByTagName("body")[0].scrollTo({
      top: 0,
      left: 0,
      behavior: 'auto'
    });
  }

  componentDidMount() {
    fetch('https://colibris-lemouvement.org/archipel-markup?domain=colibris-outilslibres.org')
      .then( (response) => {
        return response.json()
      })
      .then( (json) => {
        this.setState({
          menu: json
        })
      });
  }

  render() {
      return (
        <>
          <style>{this.state.menu.style}</style>
          <div id="archipel-colibris"
            dangerouslySetInnerHTML={{ __html: this.state.menu.markup }}
          />
          {/* <div style={{background:"#cd485f", color:"white", padding:"1em", textAlign:"center"}}>Panne majeure sur le serveur des vidéos, pads et mobilizon Colibris. Plus d'infos sur <a href="https://status.colibris-outilslibres.org">https://status.colibris-outilslibres.org</a>.</div> */}
        </>
      );
  }
}

export default ColibrisNavi
