import React from "react"
import Layout from "../components/layout"
import Link from 'gatsby-link'
import get from 'lodash/get'
import Helmet from 'react-helmet'
import LazyLoad from 'react-lazyload'
import { graphql } from "gatsby"
import CardPost from '../components/CardPost'
import CardService from '../components/CardService'

class HomeIndex extends React.Component {
  render() {
    const site = get(this, 'props.data.site.siteMetadata')
    const posts = get(this, 'props.data.remark.posts')
    const services = get(this, 'props.data.services.posts')

    // Tous les services libres
    const cardServices = []
    services.forEach((data, i) => {
      cardServices.push(
          <LazyLoad height={1} offset={500} once={true} key={i}>
            <CardService data={data.post} site={site} isIndex={true} key={i} />
          </LazyLoad>
        )
    })

    // Les 3 derniers articles
    const cardPosts = []
    posts.forEach((data, i) => {
      const layout = get(data, 'post.frontmatter.layout')
      const path = get(data, 'post.path')
      if (layout === 'post' && path !== '/404/') {
        cardPosts.push(
          <LazyLoad height={1} offset={500} once={true} key={i}>
            <CardPost data={data.post} site={site} isIndex={true} key={i} />
          </LazyLoad>
        )
      }
    })

    return (
      <Layout location={this.props.location}>
        <Helmet
          title={get(site, 'title')}
          meta={[
            { name: 'twitter:card', content: 'summary' },
            { name: 'twitter:site', content: `@${get(site, 'twitter')}` },
            { property: 'og:title', content: get(site, 'title') },
            { property: 'og:type', content: 'website' },
            { property: 'og:description', content: get(site, 'description') },
            { property: 'og:url', content: get(site, 'url') },
            {
              property: 'og:image',
              content: `${get(site, 'url')}/img/opengraph.jpg`,
            },
          ]}
        />
        <div className="intro">
          <div className="row">
            <div className="col-6 text-end">
              <h1>Organisons-nous</h1>
              <h2>et collaborons autrement !</h2>
            </div>
            <div className="col-6">
              <div className="green-txt">Outils Libres Colibris :</div>
              Des services web libres,<br />
              décentralisés, et qui respectent votre vie privée.
            </div>
          </div>
        </div>
        <div className="services">
          <div className="container">
            {cardServices}
          </div>
        </div>
        <div className="articles">
          <div className="container">
            <div className="row">
              <div className="col-md-4 col-8 text-right"><h1 className="underlined">Dernières actus</h1></div>
              <div className="col-md-8 col-4">
                <a className="rss-link" title="S'abonner au Flux RSS des actus" href="/rss.xml">
                  <i className="fa fa-rss"></i>
                </a>
              </div>
            </div>
            {cardPosts}
            <div className="row">
              <div className="col-md-4"></div>
              <div className="col-md-4">
                <Link to="/actus/" className="btn btn-primary btn-block">
                  <i className="fa fa-plus"></i> Toutes les actualités
                </Link>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    )
  }
}

export default HomeIndex

export const pageQuery = graphql`
  query IndexQuery {
    site {
      siteMetadata {
        title
        description
        url: siteUrl
        author
        twitter
      }
    }
    remark: allMarkdownRemark(
      limit:3,
      filter: {
        frontmatter:{
          type:{eq : "blog"}
        }
      },
      sort: {
        fields: [frontmatter___date],
        order: DESC
      }
    ){
      posts: edges {
        post: node {
          fileAbsolutePath
          html
          excerpt(pruneLength: 300)
          frontmatter {
            layout
            title
            path
            categories
            date(formatString: "DD.MM.YYYY")
            type
            image {
              childImageSharp {
                fixed(width: 600, height: 338) {
                  ...GatsbyImageSharpFixed
                }
              }
            }
            description
            serviceUrl
            serviceUrlTitle
          }
        }
      }
    }
    services: allMarkdownRemark(filter: {
      frontmatter:{
        type:{eq : "services"}
      }
    },
    sort: {
      fields: [fileAbsolutePath],
      order: ASC
    }) {
      posts: edges {
        post: node {
          fileAbsolutePath
          html
          excerpt(pruneLength: 300)
          frontmatter {
            layout
            title
            short
            path
            color
            image {
              publicURL
            }
            description
            serviceUrl
            serviceUrlTitle
          }
        }
      }
    }
  }
`