---
title: "N'avez-vous pas juste copié les outils de Framasoft ?"
date: "2018-01-30T22:12:03.284Z"
layout: post
type: faq
path: "/faq/n-avez-vous-pas-juste-copie-les-outils-de-framasoft/"
categories:
 - Colibris et le libre
---

**Oui et non!**  

Certains outils sont bien les mêmes que ceux proposés par Framasoft, mais comme il s'agit de logiciels libres (auxquels Framasoft contribue) on peut très bien se baser sur le même code et proposer de nouveaux hébergements de services.  

Ces outils ne sont pas forcément écrits par Framasoft, et sont proposés par un nombre croissants d'hébergeurs alternatifs, dont le [collectif Chatons](https://chatons.org), initié par Framasoft et dont Colibris Outils Libres fait partie.  

Les changements faits par Colibris sur ces outils sont aussi disponibles sous licence libre et accessibles a partir du [wiki technique des Colibris](https://colibris-wiki.org/chatons).