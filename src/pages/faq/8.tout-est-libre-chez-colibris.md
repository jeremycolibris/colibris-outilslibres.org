---
title: "Tout est libre chez Colibris ?"
date: "2018-01-30T22:12:03.284Z"
layout: post
type: faq
path: "/faq/tout-est-libre-chez-Colibris/"
categories:
 - Colibris et le libre
---

Un petit tour d'horizon de notre utilisation du libre au sein de l'équipe opérationnelle des Colibris

## Nos contenus
A l'exception des images dont la licence et les crédits sont précisés, les articles du magazine, les contenus de formations (textes et vidéos), les données des cartes sur les Oasis et les habitats participatifs, les données de la carte Près de chez nous, en gros tous nos contenus produits sont sous la licence Creative Common BY SA.

Vous pouvez donc tout réutiliser, modifier, voire en faire commerce, à condition de nous citer comme auteur, et de publier vos modifications et bonifications sous la même licence.

## Nos sites internet
Tous nos sites internet sont basés sur des logiciels libres :
 - les sites de la Fabrique, du mouvement, de l'Université, du Chant des colibris, de Pierre Rabhi sont sous Drupal
 - le site des outils libres est un site statique fait avec Gatsby
 - la boutique utilise Prestashop
 - nous utilisons beaucoup de wiki sous [YesWiki](https://yeswiki.net)

## Les services non libres que nous utilisons
Nous avons des partenaires pour certains projets qui n'ont pas développé leur solution en libre, mais ils nous accompagnent et nous font gagner beaucoup de temps, donc nous leur sommes bien reconnaissants:
 - L'espace de consultation de cap collectif
 - Glowbl pour les vidéos conférences
 - 360learning pour les parcours de formation et moocs
 - prodon pour les dons
 - les sites de crowdfunding pour la fabrique

Par ailleurs nous utilisons aussi
 - les outils de statistiques google : que pour les sites du mouvement et de la fabrique, nous utilisons dorénavant Piwik, l'alternative libre mais nous n'avons pas encore fait l'effort de tout harmoniser.
 - sendinblue pour les campagnes d'emailing
 - Les google documents et l'agenda google, mais on se met a Nextcloud et aux pads en interne, c'est juste dur de changer nos mauvaises habitudes d'un coup!

 En espérant que vous continuerez à nous aimer malgré nos imperfections.. 😅😳 

## Nos ordinateurs et équipements informatiques
Chacun est libre de configurer son ordinateur comme il le souhaite, nous avons donc des personnes qui ont franchi le pas et ont passé leur ordinateur sous Linux avec que des logiciels libres, celles et ceux qui ont gardé le système d'exploitation livré avec leur machine, mais qui utilisent des logiciels libres comme Firefox, Gimp ou VLC quotidiennement, tout le monde au sein de l'équipe est au courant des enjeux et fait sa part au mieux!