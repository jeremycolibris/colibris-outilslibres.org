---
title: "Changer de fournisseur de courrier électronique : êtes-vous prêt⋅e à vous faire la mail ?"
date: "2022-06-16T14:00:00.121Z"
layout: post
path: "/changer-de-fournisseur-de-courrier-electronique-etes-vous-pret-a-vous-faire-la-mail"
image: ./img/retrodeliverypigeon.png
type: blog
categories:
 - email
 - numérique éthique
 - GAFAM
---

*Depuis maintenant deux mois, les utilisateurs d'adresses @Colibris-lemouvement.org nous rencontrons des difficultés importantes pour envoyer des courriels chez les possesseurs de comptes gmail.**

*On va vous expliquer concrètement les conséquences de laisser les GAFAM gérer nos mails.*

## Pourquoi ?

Pourquoi donc nos mails sont rejetés car nos courriels sont *probablement* du courrier non sollicité. Paradoxal quand on essaie de répondre à nos correspondants. Et quelle est la raison ? AUCUNE IDÉE ! Nos paramètres de configuration paraissent bons, notre fournisseur de courriels nous dit que tout est bien configuré. Et bien entendu, nos appels au support de Google restent lettres mortes. Mais, tiens, pourquoi parle-t-on de Google ? Car, oui, gmail est un produit de Google. Et vous pensez bien chez Google, ils ont bien d'autres choses à faire que répondre aux colibris... \
Et en attendant, nos correspondants attendent leurs réponses. Alors, il y a bien une solution qui fonctionne et qu'on nous a soufflé : il suffit de quitter notre hébergeur d'emails et de choisir une solution qui fonctionne à coup sûr : celle de Microsoft (appelée Outlook.com), de Google (gmail.com, vous aurez suivi) par exemple.

C'est la solution la plus simple : quitter vos petits fournisseurs de courriels habituels pour migrer chez les gros .

Car de par leur positionnement central et incontournable, ce sont ces acteurs qui dictent les règles, qui font la pluie et le beau temps et qui, en rendant la vie impossible aux autres, nous incitent à choisir leurs services. [1][framasoft]

Et tout ça pour quoi ? Pour vos données. Car offrir des services de courriel est un bon moyen de capter ces données qui sont l'or noir du XXIème siècle. Non seulement celles des utilisateur⋅ice⋅s mais aussi (et c'est malheureux) mais celui de leurs correspondant⋅e⋅s, qui n'ont pas l'occasion d'accepter les conditions d'utilisation et l'exploitation des données qui vont avec !

![meme-mail-trump.jpg](img/meme-trump.jpeg)

Nous avons donc de grandes entreprises, qui gèrent nos communications en appliquant leurs règles et en exploitant les données récoltées.

Comment réagiriez-vous si votre facteur vous disait comment écrire, regardait vos échanges pour s'assurer que c'est légitime à ses yeux et en profiter pour relever les informations qu'il pourrait utiliser pour vous envoyer de la publicité ?

## Comment sortir de ces prisons dorées ?

À défaut de pouvoir agir directement, il y a une possibilité qui nous est laissée : ne plus recourir à leurs services !

Alors, on vous propose quelques alternatives qui nous semblent tenir la route, d'un point de vue technique et éthique, mais la liste est non-exhaustive !

### Quelques fournisseurs qui nous paraissent dignes de confiance

* **les CHATONS**\
  des structures réunies autour d'une charte portant des valeurs que l'on aime - majoritairement basés en France\
  <https://www.chatons.org/search/by-service?service>[\\\_type\\\_target\\\_id=112&field\\\_alternatives\\\_aux\\\_services\\\_target\\\_id=All&field\\\_software\\\_target\\\_id=All&field\\\_is\\\_shared\\\_value=All&title=](https://www.chatons.org/search/by-service?service%5C_type%5C_target%5C_id=112&field%5C_alternatives%5C_aux%5C_services%5C_target%5C_id=All&field%5C_software%5C_target%5C_id=All&field%5C_is%5C_shared%5C_value=All&title=) 
* **protonmail**\
  fait un focus sur la vie privée et le secret des correspondances - basé en Suisse\
  <https://proton.me/fr/mail/fr>
* **tutanota**\
  avec, lui aussi, un gros focus sur la sécurité et la vie privée - basé en Allemagne\
  <https://tutanota.com/fr/>
* **gandi.net**\
  un fournisseur de nom de domaine qui intègre 2 adresses emails pour chaque nom de domaine - basé en France\
  <https://www.gandi.net/fr/domain/email>
* **projet CLIC!**\
  l'auto-hébergement de vos emails en s'appuyant sur yunohost - basé chez vous !\
  <https://colibrox.colibris-outilslibres.org>

\
Ces services vous demanderont une participation financière ou participative. Cette petite contribution permet de garantir la sécurité et la pérennité de vos emails et de ces acteurs éthiques les hébergeant.

### Quelques conseils "écolos" qui peuvent paraître évidents mais qui sont toujours bons à rappeler

* envoyer le moins de courriels possibles (ce mail est-il vraiment utile?)
* avoir une signature de mail sans photo ni logo (et l'équipe Colibris doit aussi cheminer encore la dessus 😅 )
* éviter les pièces-jointes trop lourdes et inutiles (préférer les liens externes vers des espaces de téléchargement temporaires et partager ce lien dans vos messages)
* limiter au maximum les destinataires (cc et cci)
* à intervalle régulier, se désabonner aux lettres d'informations inutiles
* utiliser un logiciel libre de gestion de courriel comme [Thunderbird](https://www.thunderbird.net/fr/) : en plus de gérer par des filtres les messages, vous pourrez archiver vos anciens mails localement, voire configurer votre boite mail pour avoir moins de messages stockés en ligne
* archiver vos dossiers "Boite de réception" et "Éléments envoyés" des années précédentes ailleurs que sur votre hébergement d'emails

![meme brain](img/meme-brain.jpeg)

## Revue de liens

Fournisseurs d’emails, arrêtez de faire de la merde ! (#PasMonCaca)  
<https://framablog.org/2018/08/09/fournisseurs-demails-arretez-de-faire-de-la-merde-pasmoncaca/>  

Être un géant du mail, c’est faire la loi…\
<https://framablog.org/2017/02/17/etre-un-geant-du-mail-cest-faire-la-loi/>  

Le service e-mail de Gandi impacté par un blacklisting de l’organisation « UCEPROTECT »  
<https://news.gandi.net/fr/2021/03/le-service-e-mail-de-gandi-impacte-par-un-blacklisting-de-organisation-uceprotect/>

Réussir son changement d’adresse e-mail  
<https://www.arobase.org/messageries/changer-adresse-email.htm>  

Faut-il faire le ménage dans ses mails pour être écolo ?  
<https://www.standblog.org/blog/post/2021/03/25/Faut-il-faire-le-menage-dans-ses-mails-pour-etre-ecolo>