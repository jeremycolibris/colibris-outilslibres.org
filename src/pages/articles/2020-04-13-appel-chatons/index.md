---
title: Lettre ouverte pour des services numériques viables dans l’Éducation nationale et lʼEnseignement supérieur
date: "2020-04-13T10:00:00.121Z"
layout: post
path: "/l-appel-d-hebergeurs-alternatifs-pour-des-services-numeriques-viables-dans-l-education-nationale-et-l-enseignement-superieur"
image: ./crayon.jpg
type: blog
categories:
  - chatons
---

Avec le confinement, les outils numériques ont été largement sollicités, en particulier dans le secteur de l'éducation. Hélas, les solutions utilisées n'ont pas toujours été les plus souveraines et les plus locales, privilégiant des entreprises non francaises peu scrupuleuses du respect de la vie privées des élèves.  
Aussi, malgré des moyens techniques bien plus ambitieux que ceux du [collectif CHATONS](https://chatons.org), l'infrastructure technique de nos écoles et universités n'a pas pu tenir au moment de devoir tourner à plein régime.  
Profitons de cette crise pour encourager les services de l'état à utiliser des services libres et respectueux de la vie privée, tels que [ceux proposés par le collectif CHATONS](https://entraide.chatons.org).

-----

### À l'attention des ministères de l'Éducation nationale et de lʼEnseignement supérieur, de la recherche et de lʼinnovation

_La présente lettre est soutenue par une partie du Collectif des Hébergeurs Alternatifs Transparents Ouverts Neutres et Solidaires (CHATONS). Les organisations signataires : 42L, Alolise, ARN, ARTCODE.re, Colibris Outils Libres, Défis, Devloprog, Hadoly, Leprette.fr, Le Samarien, siick, Sleto, Sud-Ouest.org, Zici._

**Depuis le début de l’épisode de confinement, nos organisations ont constaté des dysfonctionnements aux conséquences regrettables dans l’Éducation nationale et l’Enseignement supérieur.**

Alors que le gouvernement et le CNED soutiennent qu’ils ont mis en place un système permettant la continuité des cours pour tous, la réalité semble bien différente. Les témoignages montrent la surcharge des serveurs de classes virtuelles et des Espaces Numériques de Travail, et montrent combien les enseignants sont laissés seuls pour pallier les manquements techniques des institutions nationales.

Conscient de ces manquements, la Direction Générale de l’Enseignement Supérieur est même allée jusqu’à publier un document incitant à l’usage de services mis à disposition par le monde associatif (exemple : Framasoft).

L’État a ignoré à de nombreuses reprises les appels des associations à utiliser massivement des services sous Logiciel Libre et a préféré signer un partenariat avec Microsoft en 2015. L’État a continué à dégrader les moyens disponibles dans la fonction publique. Force est de constater que cette stratégie ne permet pas de faire face à la crise actuelle.

La société civile a un rôle essentiel à tenir dans cette période difficile mais elle n’a pas vocation à être une alternative aux missions de l’Éducation nationale et de l’Enseignement supérieur. Ces institutions se doivent de garantir la continuité de la formation pour tous. Le monde associatif, vu son sous-financement et la politique désavantageuse de l’emploi à son égard, ne peut pas garantir une mobilisation suffisante pour répondre aux besoins de 870 000 enseignants et 12 800 000 élèves.

À défaut d’être correctement informés et orientés, c’est en masse qu’enseignants et élèves se tournent vers les services en ligne des GAFAM (Google, Amazon, Facebook, Apple, Microsoft). Le modèle économique de ces entreprises repose sur la collecte de données personnelles, l’enfermement numérique, l’extraterritorialité de la loi et l’opacité de leurs pratiques. Autant de menaces mettant en danger la vie privée, les données personnelles et l’intérêt général.
Nous appelons l’Éducation nationale et l’Enseignement supérieur à prendre des mesures correctives et curatives.

En renforçant les capacités de leurs services numériques, tant nationaux que locaux. C’est réalisable avec le soutien des équipes techniques et des référents numériques, l’augmentation des moyens techniques et de l’offre de services alternatifs basés sur du Logiciel Libre.

Ces derniers ont montré leur efficacité auprès d’un public varié aussi bien pour le partage de ressources, l’édition collaborative de documents que pour la visioconférence.

En informant davantage les équipes pédagogiques sur le cadre légal de l’utilisation des outils numériques et de l’incompatibilité des conditions générales d’utilisation de certains services avec le contexte de l’enseignement et sur le RGPD (Le Règlement Général sur la Protection des Données nᵒ 2016/679).

En orientant les enseignants et les élèves sur l’utilisation de services libres et décentralisés, pour garantir leur autonomie et la sécurité de leurs données personnelles.

La mise en place de ces mesures doit se faire dans le respect de certains principes, en tirant les enseignements de la situation actuelle :

- Donner la priorité au Logiciel Libre et soutenir son développement.
- Assurer la gestion des services en interne.
- S’appuyer sur les équipes de terrain.
- Interdire toute obligation d’usage de services exploitant la vie privée des élèves ou ne respectant pas le RGPD, notamment ceux des GAFAM.
- Informer correctement sur les contraintes réglementaires à respecter par les enseignants et les élèves.
