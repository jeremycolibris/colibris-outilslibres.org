---
title: Les humains derrière les machines
date: "2017-02-07T22:40:32.169Z"
layout: post
type: blog
path: "/les-humains-derriere-les-machines/"
image: ./stefan-stefancik-257625.jpg
categories:
  - présentation
---

_Toute une équipe de bénévoles et des membres de l'équipe opérationnelle des Colibris sont mobilisés sur le projet des outils libres. Nous vous les présentons ici brièvement par ordre alphabétique._


<div class="row">
  <div class="col photo">
    <img src="./christophe.jpg" alt="christophe" />
  </div>
  <div class="col-9">
    <h2>Christophe , le graphiste</h2>
    Christophe est graphiste en freelance et s'occupe du redesign et de l'harmonisation graphique de l'ensemble des sites du mouvement Colibris. C'est donc lui qui a créé le design de ce site et des services associés.
  </div>
</div>


-----


<div class="row">
  <div class="col photo">
    <img src="./florian.jpg" alt="florian" />
  </div>
  <div class="col-9">
    <h2>Florian, le développeur</h2>
    Florian est le coordinateur technique du projet Outils Libres et l'informaticien officiel des Colibris, travaillant pour le pole Soutenir de l'association. Il programme beaucoup en php et javascript, est le co-auteur de YesWiki, mais fais aussi de l'administration de serveur sous linux.
  </div>
</div>


-----


<div class="row">
  <div class="col photo">
    <img src="./jean-philippe.jpg" alt="jean-philippe" />
  </div>
  <div class="col-9">
    <h2>Jean-Philippe, l'architecte système</h2>
    Jean-Philippe est un ingénieur système de haute volée qui depuis presque 2 ans, participe au projet de manière bénévole et a mis en place toute l'architecture serveur pour que les services disponibles soient sécurisés, évolutifs, cloisonnés et sauvegardés. Un grand merci pour sa grande contribution au projet!
  </div>
</div>


-----


<div class="row">
  <div class="col photo">
    <img src="./jean-marie-et-sylvere.jpg" alt="jm et sylvere" />
  </div>
  <div class="col-9">
    <h2>Jean-Marie et Sylvère, les compagnons oasis</h2>
    Ces 2 compagnons Oasis, ont su développer une double compétence : à la fois un savoir faire pour les projets d'oasis et des compétences techniques pour avoir les outils disponibles pour s'organiser au mieux. Forts de leur expérience, ils accompagnent maintenant aussi les nouveaux venus sur la ferme à wikis!
  </div>
</div>


-----


<div class="row">
  <div class="col photo">
    <img src="./mathieu.jpg" alt="mathieu" />
  </div>
  <div class="col-9">
    <h2>Mathieu, le chef d'orchestre</h2>
    En plus de sa fonction de premier lien de Colibris et ses nombreux engagements associatifs, Mathieu a pris sur son temps pour épauler et apporter ses compétences de gestion de projet et recherche de financement au projet Outils Libres. Par ailleurs, il impulse et forme des personnes à l'utilisation d'outils libres comme les wikis dans divers secteurs.
  </div>
</div>


-----


<div class="row">
  <div class="col photo">
    <img src="./louise.jpg" alt="louise" />
  </div>
  <div class="col-9">
    <h2>Louise, la quincaillère</h2>
    Concevoir, imaginer, bricoler, construire, fabriquer, consolider à plusieurs et avec plaisir nécessite, aujourd’hui comme hier, des méthodes et des outils ! Accompagnatrice et formatrice en projet coopératifs, Louise apporte son expertise à l'ensemble des groupes locaux au sein du Pôle Relier de Colibris, vous la verrez sans doute dans nos parcours d'initiation aux outils! 
  </div>
</div>


-----



<div class="row">
  <div class="col photo">
    <img src="./sebastian.jpg" alt="sebastian" />
  </div>
  <div class="col-9">
    <h2>Sebastian, créateur de Gogocarto</h2>
    Gogocarto est le logiciel libre que Sebastian a développé bénévolement depuis 2 ans, pour la carte collaborative <a href="https://presdecheznous.fr">Près de chez nous</a>. Le projet suit son cours et son implication nous permet d'envisager peut-être déjà cette année ce logiciel comme une service d'Outils libres pour que chacune et chacun puisse créer ses propres cartos participatives !
  </div>
</div>


-----

Photo de la vignette de l'article sous licence CC0 de [Štefan Štefančík](https://unsplash.com/photos/UCZF1sXcejo)