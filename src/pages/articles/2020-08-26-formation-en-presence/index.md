---
title: "FORMATION : 3 jours pour réaliser votre site web collaboratif libre et éthique"
date: "2020-08-26T16:00:00.121Z"
layout: post
path: "/formation-en-presence-outils-numeriques-et-animation-de-collectif"
image: ./img/formation.jpg
type: blog
categories:
 - formation
 - en présence
 - projets coopératifs
---

Rendez-vous au château de **Jambville (50 km de Paris)**, en amont du [festival Oasis](https://cooperative-oasis.org/wiki/?InfosFestival2020), **du lundi 28 septembre au mercredi 30 septembre** pour avoir votre dispositif internet collaboratif opérationnel.  

Travailler sur son **propre projet Internet** collaboratif **éthique et libre**, savoir **l’animer** et **le faire évoluer** sans être informaticien, c’est possible !  

![formation](/img/formation.jpg)

## La formation

Nous vous proposons **3 jours d’ateliers accessibles** à toute personne souhaitant aller un cran plus loin dans l’autonomie et la gestion de ses sites Internet, mobiliser son collectif, animer et développer des projets collaboratifs avec la possibilité de les faire évoluer en fonction des besoins qui émergent.

L’idée de cette formation est **d’analyser en amont vos besoins et l’état d’avancement de votre projet pour vous proposer des solutions libres et éthiques adaptées**, afin qu’à l’issue de ces 3 jours de formation :

- votre plateforme Internet soit en ligne et prête à fonctionner uniquement avec des logiciels libres les plus éprouvés (YesWiki, Wordpress, Piwigo, ...),
- vous soyez capables de la faire évoluer de manière indépendante,
- vous puissiez vous en servir comme outil d’animation d’un collectif.

## Le programme

Avant le début de la formation, nous prendrons contact avec vous pour détailler vos besoins et élaborer un **programme sur-mesure**. Nous proposerons ensuite plusieurs ateliers répartis sur les 3 jours.
Pas besoin d’être spécialiste en informatique pour suivre cette formation, mais avoir quelques affinités avec Internet et des bases de html peuvent aider.

Vous trouverez ci-dessous la proposition initiale de programme, que nous adapterons en fonction de vos besoins :

- Lundi 28 septembre :
  - le glossaire du web (comprendre des mots comme "cloud", "dns", "html", "css", "ftp", "php", "mysql", "docker", ..)
  - choisir son hébergement et son nom de domaine
  - conseils pour bien démarrer son projet en ligne avec son collectif
  - panorama des services libres pour démarrer son projet (prise de notes, votes, visio-conférence, listes de discussions,... )
  - installation d'outils libres sur son poste de travail (windows, macOs ou linux)
  - aspects juridiques de l'internet
- Mardi 29 septembre :
  - installation d'outils libres sur son espace internet à soi
  - personnaliser le graphisme de son site internet (bases de html et css)
  - réaliser la gare centrale (point d'entrée unique du projet) de votre projet collectif
  - les licences libres et le partage sincère
- Mercredi 30 septembre :
  - outils de veille, de syndication, de curation (les flux et reflux)
  - communiquer / diffuser sur son projet (newsletter, réseaux sociaux)
  - ateliers techniques à la carte

## Les formateurs

![Laurent et Gatien en conversation](/img/laurent.jpg)

<div class="text-center" style="margin-top:-1em;margin-bottom:1em;"><small><em>Laurent animant une conférence</em></small></div>

### Sur le volet méthodologique et pédagogique

Laurent Marseault est [secoueur de cocotiers](https://cocotier.xyz), pompier volontaire et, pour beaucoup aujourd’hui, maître Jedi de la coopération.  
Il accompagne et forme des collectifs avec des [outils et méthodes d’animations](https://cocotier.xyz/?BoiteAOutils) faciles à appréhender depuis 20 ans, piochant ses idées dans l'observation du vivant, comme dans son précédent métier d'animateur nature.

### Sur le volet technique

Florian Schmitt, développeur web et administrateur système indépendant, chargé de mission pour le mouvement Colibris, responsable du projet [Colibris Outils Libres](https://colibris-outilslibres.org).  
Co-créateur de [YesWiki](https://yeswiki.net), ancien salarié de l'association Outils Réseaux.
Développe particulièrement sous php et javascript, avec une attention particulière au design d'interfaces simples et accessibles, très faciles d'utilisation, et si possible jolies!

## Logistique et coût

### Nombre de participants

La formation est prévue pour **8 à 15 participants**.

### Tarif

**400€** pour les 3 jours (tarif éligible à la formation professionnelle OPCO).  
_Si le tarif est trop élevé pour votre venue, possibilité de demander un tarif solidaire._

⚠️ Prévoir aussi frais d'hébergement et de nourriture (pension complète) en plus (qui peuvent potentiellement être remboursés par votre OPCO):

- Journées en semaine en chambre 1 à 4 + repas : **165 euros** pour les 3 jours
- Journées en semaine en dortoir de 5 à 20 + repas : **129 euros** pour les 3 jours
- Journées en semaine en camping (prévoir sa tente) + repas : **105 euros** pour les 3 jours

**Pour un total de 565 euros max, passez 3 jours en immersion, réalisez et maitrisez votre dispositif Internet : c'est moins cher qu'une seule journée de prestation, et cela rendra plus autonome que jamais !**

### Où ?

Au **château de Jambville** (1h de Paris), [voir le plan d'accès](https://jambville.sgdf.fr/images/stories/jambville/acces%20jambville.pdf).  
L'hébergement (sous tente, en dortoir, ou en chambres 3 ou 4 personnes) et la pension complète, se feront sur place.  
En bonus : vous aurez la possibilité d’enchainer sur le [festival oasis](https://www.cooperative-oasis.org/festivaloasis2020/) !

### Quand ?

**Lundi 28 septembre 9h du matin au mercredi 30 septembre 18h**.  
Il est demandé d’arriver le dimanche soir.

## Je m'inscris tout de suite !

*⚠️ Clôture des inscriptions le 24 septembre.*

<iframe width="100%" height="800" frameborder="0" src="https://colibris-wiki.org/chatons/?InscriptionFormationOasis/iframe"></iframe>
