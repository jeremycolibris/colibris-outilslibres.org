---
title: "Sortie de GoGoCarto : cartographier les territoires alternatifs"
date: "2020-06-23T16:00:00.121Z"
layout: post
path: "/sortie-de-gogocarto-cartographier-territoires-alternatifs"
image: ./img/gogocarte.jpg
type: blog
categories:
  - services
  - annonces
  - cartographie
---
![mappe monde](/img/gogocarte.jpg)

> “_A l’échelle d’une carte, le monde est un jeu d’enfant._” Laurent Graff

*Article de Floriane Hamon et Florian Schmitt, paru [dans Colibris le Mag](https://www.colibris-lemouvement.org/magazine/cartographier-territoires-alternatifs), le 22 juin 2020*.

-----

Dernier **outil libre de Colibris** à sortir officiellement : [GoGoCarto](https://gogocarto.fr/projects) est un **logiciel libre** permettant de créer des **annuaires cartographiques collaboratifs**. Il a été conçu comme un service libre en ligne, c’est à dire que n’importe qui peut l’utiliser pour créer sa propre carte, avec sa propre configuration (textes, couleurs, fonctionnalités), ses propres données et ses propres règles de modération.

-----

## La création d'un nouveau service de cartographies contributives

En 2017, le site [Près de Chez Nous](https://presdecheznous.fr/) est lancé : une carte collaborative en logiciel libre qui recense les produits ou services de proximité dédiés à l'écologie et à la solidarité. Cette carte reprend le travail réalisé par Colibris et le Marché Citoyen, et enrichi par de nombreux contributeurs et contributrices pendant 10 ans. 

Cette nouvelle carte a aussi vu le jour grâce au développement par Sebastian Castro d'un nouveau logiciel libre de cartographies collaboratives : GoGoCarto. À l'époque, Sebastian fait le constat qu'il n'existe pas d'alternative à Google Mymaps qui soit libre, joli, ergonomique et rapide. 

![Près de chez nous](/img/pdcn.png)

Il passe donc plusieurs mois à développer ce logiciel pour avoir une interface agréable, un système de classification clair et précis, ainsi qu'un système de contribution et validation collaboratives.

Si Près de Chez Nous est le premier site web créé avec GoGoCarto, il est assez vite rejoint par [Transiscope](https://transiscope.org), un collectif d'acteurs de la transition écologique et sociale qui agrège différentes bases de données sur les alternatives citoyennes.

Voyant le succès de ce logiciel de cartographie, Colibris a décidé de le proposer comme service libre. [GoGoCarto](https://gogocarto.fr) permettra ainsi à de nombreux projets de bénéficier de cette technologie.

## Pendant le confinement, des cartes à gogo !

Le service a donc démarré, sans communication officielle, mais par bouche à oreille au sein de la communauté du logiciel libre, dont l'[Assemblée Virtuelle](https://www.virtual-assembly.org/), et la liste de discussion APRIL, et au sein des réseaux de la transition écologique et sociale. Rapidement une centaine de cartes sur des sujets variés allant de la [transition écologique en Belgique](https://lacartedestransitions.gogocarto.fr/annuaire) aux [fripes françaises](https://tptf.gogocarto.fr/annuaire) en passant par [les ponts dans le monde](https://technopont.gogocarto.fr/annuaire) et les [rond-points en Bretagne](https://rondspointsbretagne.gogocarto.fr/annuaire) apparaissent.

![meme confinement](/img/meme-conffinement.jpg)

La communauté s'organise autour d'un [canal sur la plateforme tchat des Communs](https://chat.lescommuns.org) et permet aux usagers de solliciter les développeurs. Des vidéos de tutoriels sont créées pour [faciliter la compréhension de l'interface d'administration](https://video.colibris-outilslibres.org/video-channels/gogocarto_channel/videos).  

Fin d'année 2019, plus de 400 cartes étaient déjà créées.

La période du confinement a accéléré la création de nouvelles cartes... comme la [carte initiée par le CIVAM du Gard](https://circuitscourts.gogocarto.fr), utile pour trouver des producteurs qui maintiennent leur activité en circuits courts, ou la carte de [Covid-entraide](https://covidentraide.gogocarto.fr/annuaire).

Mi mai 2020, à la fin du confinement, nous comptions près de de 800 cartes.

## Encore une carte, est-ce bien raisonnable ?

![meme bouton rouge](/img/meme-bouton-rouge.png)

Sur les 800 cartes présentes sur GoGoCarto fin mai 2020, seules 250 ont été conservées dans la version officielle du site, car peu d'entre elles étaient encore actives.

En effet, l'outil, bien que très plaisant, n'est pas une solution à tout. Pour qu'une cartographie fonctionne, il faut l'animer, maintenir les données existantes, faire en sorte que de nouveaux points soient régulièrement ajoutés. Faire vivre une carte dans la durée demande des moyens humains et financiers, ainsi que de la persévérance. De nombreux projets voient donc le jour et s'essoufflent rapidement. En outre, plusieurs cartes peuvent traiter de sujets similaires ou peuvent se concentrer sur une échelle de territoire trop restreinte : dans ces deux cas de figure, les cartes ont des durées de vie assez courtes.

Avant de démarrer une cartographie, il est donc proposé aux porteurs de projet et initiateurs de cartes de se poser quelques questions pour être sûr que ça vaut la peine de consacrer de l'énergie et du temps de vie à ce nouveau projet. C'est parfois l'occasion de découvrir des projets existants assez proches de son idée initiale et de devenir contributeur plutôt que de créer une nouvelle carte.

## Les quelques questions à se poser avant d'initier une nouvelle cartographie

- Aller voir sur [gogocarto.fr](https://gogocarto.fr/projects) s'il n'existe pas des cartes qui ressemblent à ce que vous souhaiteriez créer, et le cas échéant, contactez les gens pour devenir contributeur ;  
- Chercher sérieusement sur internet si une carte similaire n'existe pas déjà : sur votre moteur de recherche non-gafam préféré, sur l'instance [umap](https://umap.openstreetmap.fr/fr/) (parmis plus de 400 000 cartes), sur [framacarte](https://framacarte.org/fr) et aussi la source de toutes les cartes [OpenStreetMap](https://www.openstreetmap.org).  
- Se demander de quelles ressources on dispose pour la maintenance des données. Cette dernière suppose, sur le long terme, selon nous, l'implication d'une communauté de contributeurs et contributrices. Il y a fort à parier que faire un projet dans son coin, sans réflexion collective, notamment sur la raison d'être et les intentions de la carte, ne saurait être pérenne...

## Pour aller plus loin  

Pour découvrir toutes les cartographies existantes et éventuellement créer de nouveaux projets, rendez-vous sur [gogocarto.fr](https://gogocarto.fr)

-----

*Crédit Photo : de slon_dot_pics provenant de Pexels*
